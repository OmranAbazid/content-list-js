
// taken from : https://stackoverflow.com/questions/22780430/javascript-xmlhttprequest-using-jsonp
/**
 * inject a script tag into the dom the request URL
 * returns a callback function on response
 * @param {*} url 
 * @param {*} callback 
 */
export function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(false, data);
    };
  
    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
    script.onerror = function (err) {
        callback(err);
    }
}