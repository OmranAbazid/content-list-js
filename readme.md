# Content-list
A widget that lists the content items from Idio API

## Build setup

``` bash
# install dependencies
npm install

# serve with auto reload at localhost:8000
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run test

```

## Technology used
- Javascript, HTML, CSS
- Testing is done using karma and mocha
    -   Karma: test runner to be able to test html element using phantomjs
    -   Mocha: testing framework

## Reasons for choosing the technology
- This project is apropriate for small projects 
because it only uses basic HTML, css, javascript
- For the unit testing mocha and karma intergrate well with javascript
and more importantly they are easy to setup

## Possible improvements
- Add test coverage 
- Add more unit tests
- Generate everthing in the build folder instead 
of only generating the bundle