/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(4);
module.exports = __webpack_require__(5);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Card = __webpack_require__(2);

var _Card2 = _interopRequireDefault(_Card);

var _utils = __webpack_require__(3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// constants
var URL = 'https://api.idio.co/1.0/content';
var KEY = 'URN4QXKCG3QD3Y5MS51A';
// dom variables
var $appContainer = document.getElementById('app');
/**
 * return html list of items
 * it filters the items to show only the one that has images
 * then convert then to html cards and concatinate them
 * @param {Array} contentList 
 */
function getCards(contentList) {
  return contentList.filter(function (item) {
    return item['main_image_url'];
  }).map(function (item) {
    return (0, _Card2.default)(item);
  }).join('');
}

/**
 * issues a jsonp request to get the data
 * pass it to getCards function to return the HTML
 * inject the contentlist html into the dom
 */
function render() {
  (0, _utils.jsonp)(URL + '?key=' + KEY, function (res) {
    var cardsHTML = getCards(res.content);
    $appContainer.innerHTML = cardsHTML;
  });
}

render();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getCard;
function getCard(item) {
    return '<a href="' + item['link_url'] + '"\n        target="_blank" \n        class="card"\n    >\n    <div \n        style="background-image: url(' + item['main_image_url'] + '?w=350&h=240)" \n        class="card-image"\n    >\n    </div>\n    <h3>' + item.title + '</h3>\n    </a>';
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.jsonp = jsonp;

// taken from : https://stackoverflow.com/questions/22780430/javascript-xmlhttprequest-using-jsonp
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function (data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
}

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = "body {\r\n    padding: 0px;\r\n    margin: 0px;\r\n    box-sizing: border-box;\r\n    background: #f9f9f9;\r\n  }\r\n  \r\n  #app {\r\n    margin: 0 auto;\r\n    width: 350px;  \r\n  }\r\n\r\n  a.card {\r\n    display: block;\r\n    width: 350px;\r\n    margin-top: 30px;\r\n    background: #fff;\r\n    -webkit-box-shadow: 2px 2px 5px -1px rgba(173, 173, 173, 1);\r\n    -moz-box-shadow: 2px 2px 5px -1px rgba(173, 173, 173, 1);\r\n    box-shadow: 2px 2px 5px -1px rgba(173, 173, 173, 1);\r\n    color: #555555;\r\n    text-decoration: none;\r\n  }\r\n  \r\n  a.card:hover {\r\n    color: #f8116a;\r\n  }\r\n  \r\n  div.card-image {\r\n    width: 350px;\r\n    height: 240px;\r\n    background-size: cover;\r\n  }\r\n  \r\n  h3 {\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    font-weight: 900;\r\n    text-transform: uppercase;\r\n    padding: 25px;\r\n    margin: 0px;\r\n  }"

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\r\n</head>\r\n\r\n<body>\r\n    <div id=\"app\"></div>\r\n    <script src=\"app.bundle.js\"></script>\r\n</body>\r\n</html>"

/***/ })
/******/ ]);
//# sourceMappingURL=app.bundle.js.map