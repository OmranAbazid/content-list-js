import getCard from './views/Card.js'
import { jsonp } from './utils.js'
// constants
const URL = 'https://api.idio.co/1.0/content'
const KEY = 'URN4QXKCG3QD3Y5MS51A'
// dom variables
const $appContainer = document.getElementById('app');
const $error = document.getElementById('error');
/**
 * return html list of items
 * it filters the items to show only the one that has images
 * then convert then to html cards and concatinate them
 * @param {Array} contentList 
 */
function getCards(contentList) {
 return contentList
  .filter((item) => item['main_image_url'])
  .map((item) =>getCard(item)).join('');
}

/**
 * issues a jsonp request to get the data
 * pass it to getCards function to return the HTML
 * inject the contentlist html into the dom
 */
function render() {
  jsonp(URL + '?key=' + KEY, (err, res) => {
    if(!err) {
      let cardsHTML = getCards(res.content);
      $appContainer.innerHTML = cardsHTML;
      return;
    }
    $error.innerHTML = 'Error occured in the request';
  })
}

render();

