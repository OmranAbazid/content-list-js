import getCard from '../js/views/Card.js';
import assert from 'assert';

let item = {
  title: 'testTitle',
  main_image_url: 'http://google.com/image.jpg',
  link_url: 'http://www.google.com',
};

describe('Cards.js', function() {
  var itemHTML = getCard(item);
  var el = document.createElement('html');
  el.innerHTML = itemHTML;

  it('should return correct title', () => {
    assert.equal(el.querySelector('a h3').textContent,
      'testTitle');
  });

  it('should render correct link', () => {
    assert.equal(el.querySelector('a').getAttribute('href'),
      'http://www.google.com');
  });

  it('should render correct image', () => {
    var itemHTML = getCard(item);
    var el = document.createElement('html');
    el.innerHTML = itemHTML;
    assert.equal(el.querySelector('a div').getAttribute('style'),
      'background-image: url(http://google.com/image.jpg?w=350&h=240)');
  });
});
