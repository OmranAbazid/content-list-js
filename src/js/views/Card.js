
/**
 * return the html of a sigle card
 * @param {*} item 
 */
export default function getCard(item) {
    return `<a href="${item['link_url']}"
        target="_blank" 
        class="card"
    >
    <div 
        style="background-image: url(${item['main_image_url']}?w=350&h=240)" 
        class="card-image"
    >
    </div>
    <h3>${item.title}</h3>
    </a>`;
}
